<?php
/**
 * Functions handling registering new CSS template post type and its associated metaboxes
 */

add_action( 'init', 'geoproject_register_post_types', 10, 1 );
/**
 * Registers Book Template post type
 */
function geoproject_register_post_types(){
    register_post_type( 'pagedjs-template',
        array(
            'labels'               => array(
                'name'          => __( 'Book Templates', 'geoprint' ),
                'singular_name' => __( 'Template', 'geoprint' ),
                'add_new'       => __( 'Create a new template', 'geoprint' ),
                'add_new_item'  => __( 'Create a new template', 'geoprint' ),
                'edit_item'     => __( 'Edit book template', 'geoprint' ),
            ),
            'exclude_from_search'  => true,
            'has_archive'          => false,
            'menu_position'        => 50,
            'public'               => true,
            'supports'             => array( 'title' ),
            'menu_icon'            => 'dashicons-book-alt',
            'register_meta_box_cb' => 'geoproject_book_template_metabox'
        )
    );
}


/**
 * Registers our CSS template metabox
 */
function geoproject_book_template_metabox(){
    add_meta_box( 'css_metabox_custom_fields', __( 'PagedJS template settings', 'geoprint' ),  'geoproject_book_template_metabox_callback', 'pagedjs-template' );
}


/**
 * CSS template metabox display callback
 */
function geoproject_book_template_metabox_callback( $post ){
    $templateCSS = get_post_meta( (int) $post->ID, 'templateCSS', true );
    wp_nonce_field( 'geoprint_save_css', 'geoprint_save_css_nonce', true, true );
    ?>
        <label for="templateCSS"><?php esc_html_e( 'CSS code', 'geoprint' ); ?></label>
        <textarea placeholder="CSS code here" name="templateCSS" rows="30" class="widefat"><?php print esc_textarea( $templateCSS );?></textarea>
    <?php
}


add_action( 'save_post_pagedjs-template', 'geoprint_template_save' );
/**
 * Saves CSS template
 */
function geoprint_template_save( $post_id ){
    if ( ! isset( $_POST['geoprint_save_css_nonce'] ) || ! wp_verify_nonce( $_POST['geoprint_save_css_nonce'], 'geoprint_save_css' )) return;
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if ( ! current_user_can( 'edit_post', (int) $post_id  ) ) return;
    if ( isset( $_POST['templateCSS'] ) ) update_post_meta( (int) $post_id, 'templateCSS', sanitize_textarea_field( $_POST['templateCSS'] ) );
}