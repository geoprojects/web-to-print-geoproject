<?php
/**
 * Registers metaboxes for projects
 */

add_action( 'add_meta_boxes', 'geoproject_projects_metaboxes' );
/**
 * Registers print settings metabox.
 */
function geoproject_projects_metaboxes( $post_type ) {
    if( 'projects' === $post_type ){
        add_meta_box( 'project_print_settings', __( 'Print Settings', 'geoprint' ), 'geoproject_print_settings_callback', 'projects', 'advanced', 'default' );
        add_meta_box( 'project_colophon_settings', __( 'Colophon', 'geoprint' ), 'geoproject_colophon_settings_callback', 'projects', 'advanced', 'default' );
    }
}

/**
 * Print settings metabox display callback
 * 
 * @param  WP_Post  $post
 * @param  array    $args
 */
function geoproject_print_settings_callback( $post, $args ){
    wp_nonce_field( 'geoprint_save_settings', 'geoprint_save_settings_nonce', true, true );

    geoprint_css_template_input( (int) $post->ID );
    geoprint_content_order_inputs( (int) $post->ID );

    $preview_url = add_query_arg( 'paged', 'yes', get_permalink( (int) $post->ID ) ); 
    printf( '<a href="%s" class="button" target="_blank">%s</a>', esc_url( $preview_url ), esc_html( 'Preview !', 'geoprint' ) );
}


/**
 * Displays colophon content fields
 * 
 * @param  WP_Post  $post
 * @param  array    $args
 */
function geoproject_colophon_settings_callback( $post, $args ){
    $content = get_post_meta( (int) $post->ID, 'geoprint_colophon_content', true );
    wp_editor($content, 'geoprint_colophon_content');
}


add_action( 'save_post_projects', 'geoproject_print_settings_save' );
/**
 * Saves our metabox settings
 */
function geoproject_print_settings_save( $post_id ){
    if( ! isset( $_POST['geoprint_save_settings_nonce'] ) || ! wp_verify_nonce( $_POST['geoprint_save_settings_nonce'], 'geoprint_save_settings' ) ) return;
    if( 'projects' !== get_post_type( $post_id ) ) return;
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if( ! current_user_can( 'edit-post', (int) $post_id  ) ) return;

    $chosen_template = isset( $_POST['chosenTemplate'] ) ? (int) $_POST['chosenTemplate'] : 0;
    update_post_meta( $post_id, 'chosenTemplate', $chosen_template);

    $colophon = isset( $_POST['geoprint_colophon_content'] ) ? $_POST['geoprint_colophon_content'] : '';
    update_post_meta( $post_id, 'geoprint_colophon_content', $colophon );
    
    $contents = geoprint_get_project_content( (int) $post_id, [ 'fields' => 'ids' ] );
    if( ! empty( $contents ) && is_array( $contents ) ){
        foreach ( $contents as $index => $id ) {
            if( isset( $_POST[ "paged-order-${id}" ] ) ) update_post_meta( (int) $id, 'paged-order', (int) $_POST[ "paged-order-${id}" ] );
        }
    }
}

