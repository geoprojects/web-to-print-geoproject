# Geoprint

Cette extension pour WordPress permet d'imprimer des livrets à partir de récits GeoProjects.

## Utilisation principale

Dans l'administration de WordPress, sur la page d'édition d'un récit, vous disposez d'une nouvelle métabox permettant de changer l'ordre d'impression et de séléctionner un modèle d'impression CSS.

![screenshot-1.png](screenshots/screenshot-1.png)

En cliquant le bouton *Preview* vous pouvez avoir un aperçu de l'impression, et vous disposez sur la gauche de toute une série de régales, divisés en plusieurs sections :

![screenshot-2.png](screenshots/screenshot-2.png)

* **Réglages de Contenu** : la section contenu regroupe les réglages généraux concernant le contenu à afficher, l'ordre dans lequel l'afficher, ainsi que les modèles de formats et CSS à utiliser. Le colophon est une simple boite de texte riche à compléter sur la page d'édition du récit, sous les réglages d'ordre d'impression.

* **Réglages de base** : Les réglages de base gouvernent tous les réglages de couleur, typographie, et marges qui seront appliqués par défaut à l'ensemble du livret imprimé.

Toutes les autre sections ont globalement les mêmes réglages, mais qui s'appliqeront uniquement au type de contenu concerné. Par exemple, les réglages des cartes n'affecteront que les contenu de type "Maps".

Le bouton *Imprimer* permet de sauvegarder en pdf ou d'imprimer le livret en passant pas la fonction d'impression du navigateur, et en utilisant les réglages systèmes de l'ordinateur utilisé.

Attention à ne pas oublier de cocher "Graphiques d'arrière-plan" pour imprimer les arrière-plans des pages et les couleurs de fond.

## Organisation des fichiers de l'extension.

L'extension est organisée de manière simple : 

 * `build/` contient les fichiers CSS et JS minifié. Ne pas y toucher !
 * `src/` contient les fichiers SCSS et JS source à partir desquels sont compilés les fichiers `build/`
 * `inc/` contient les fichiers PHP de l'extension et toutes ses fonctions.
 * `languages/` contient les traductions
 * `templates/content` contient les modèles utilisés pour afficher les contenus du récit, selon leur type.
 * `formats` contient les fichiers CSS des formats de base d'impression disponibles. Dû à une contrainte technique (gestion de la propriété CSS non-standard `@page`), le format du livre est obligatoirement à passer via une "vraie" feuille de style CSS.
 * `icons/` contient les fichiers svg des motifs d'arrière-plan.

## Modifier un modèle ou un format

**Vous ne devez pas modifier les fichiers de l'extension.**

Cette dernière est conçue pour pouvoir être étendue via les hooks de WordPress et le même système de modèles et de thème enfant.

### Pour modifier un modèle

Si vous avez besoin de modifier un modèle pour ajouter une information : 

 * Dans votre thème enfant, créez un dossier `geoprint/`.
 * Copiez-collez le fichier que vous souhaitez modifier depuis l'extension vers le dossier `geoprint/`, **en respectant la structure des dossiers**, exactement comme pour surcharger un modèle de thème. Par exemple, si vous voulez ajouter une information dans le modèle des articles, copiez-collez le fichier `templates/content/post.php` dans le dossier `geoprint/content/` de votre thème enfant.
 * Effectuez vos modifications dans le modèle dans votre thème enfant.

Les modèles du thème enfant sont pris en priorité sur ceux de l'extension.

### Pour ajouter un format

Un format d'impression est **simplement un fichier CSS avec une déclaration CSS `@page`**.

Pour ajouter un nouveau format d'impression : 

 * Si ce n'est pas déjà fait, créez un dossier `geoprint/formats/` dans votre thème enfant.
 * Créez une feuille de style css avec la déclaration `@page` correspondante.

Puis ajoutez ce snippet dans votre fichier `functions.php` :

```php
add_filter( 'geoprint_book_formats', 'my_prefix_book_formats', 10, 1 );
/**
 * Adds a new format
 */
function my_prefix_book_formats( $formats ){
    $formats['newformat.css'] = __( 'New format', 'geoprint' );
    return $formats;
}
```

Remplacez `'newformat.css'` par le nom de votre fichier, et `'New format'` par le nom que vous souhaitez lui donner dans la colonne des réglages.

L'extension scanne automatiquement le contenu de votre dossier `geoprint/formats/`, mais si le format n'est pas déclaré via le snippet précédent, il n'apparaitra pas.

Alternativement, vous pouvez aussi directement ajouter la feuille de style dans le dossier `formats/` de l'extension, et ajouter le label du format dans la fonction `geoprint_get_book_formats()` dans le fichier `functions.php`.

### Ajouter une police de caractères

Dans le fichier `functions.php` de votre thème enfant, ajoutez :

```php
add_filter( 'geoprint_font_families', 'my_prefix_font_families', 10, 1 );
/**
 * Registers a new font family
 */
function my_prefix_font_families( $families ){
    $families['my-family'] = array(
        'label'   => __( 'My family', 'geoprint' ),
        'src'     => 'https://fonts.googleapis.com/css2?family=...',
        'value'   => "'My Family', sans-serif",
    ),
    return $families;
}
```

* `my-family` est un identifiant texte que vous pouvez choisir.
* `label` est le nom qui apparaitra dans les réglages.
* `src` est l'URL à laquelle télécharger la police.
* `value` est la valeur de la propriété CSS `font-family` à utiliser.

### Ajouter un motif de fond

Dans le fichier `functions.php` de votre thème enfant, ajoutez :

```php
add_filter( 'geoprint_bg_images', 'my_prefix_bg_images', 10, 1 );
/**
 * Registers a new background image
 */
function my_prefix_bg_images( $images ){
    $images['my-image'] = array(
        'label'   => __( 'My image', 'geoprint' ),
        'src'     => trailingslashit( GEOPRINT_URL ) . 'icons/topography.svg',
    ),
    return $families;
}
```

* `my-image` est un identifiant texte que vous pouvez choisir.
* `label` est le nom qui apparaitra dans les réglages.
* `src` est l'URL à laquelle télécharger le SVG à utiliser.
