<?php
/**
 * Template for displaying project header
 */
$project_id = get_the_ID();
$image_url = get_the_post_thumbnail_url( $project_id, 'gp-project-thumb', false );
?>
<section class="project" data-title="<?php echo esc_attr( the_title() ); ?>">
	<style>
		.pagedjs_project_first_page {
			background-image: url("<?php echo esc_url( $image_url ) ?>") !important;
		}
	</style>

	<header class="project-header">
		<h1 class="project-title"><span><?php the_title(); ?></span></h1>
	</header>

	<div class="project-content">
		<?php the_content(); ?>
	</div>

	<?php
		$project_owner   = get_post_meta( $project_id, 'gp_owner', true );
		$project_website = get_post_meta( $project_id, 'gp_website', true );

		if ( $project_owner != '' ) : ?>
			<p class="project-owner"><span><?php echo $project_owner; ?></span></p>
		<?php endif;
	?>
</section>