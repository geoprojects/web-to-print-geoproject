<?php
/**
 * Template for displaying geoformat
 */

$id = get_the_id();
$meta_auteur = get_post_meta( $id, 'meta-auteur', true );
$subline     = get_post_meta( $id, 'meta-subline', true );
$chapo       = get_post_meta( $id, '_wp_editor_chapo', true );
$txtFacultatif = get_post_meta( $id, '_wp_editor_text', true );
$paged_order = get_post_meta( $id, 'paged-order', true );
$image_url = get_the_post_thumbnail_url( $id, 'gp-project-thumb', false );

?>

<section class="geoformat" data-order="<?php echo (int) $paged_order; ?>" data-title="<?php echo esc_attr( the_title() ); ?>">
	<style>
		.pagedjs_geoformat_first_page {
			background-image: url("<?php echo esc_url( $image_url ) ?>") !important;
		}
	</style>
	
	<div class="geoformat-intro" data-order="<?php echo (int) $paged_order; ?>">
		<h1 class="titre"><?php the_title() ?></h1>
		<?php 
			if ( $subline ) printf( '<p class="subline">%s</p>', wp_kses_post( $subline ) );
			if ( $chapo ) printf( '<div class="chapo">%s</div>', wp_kses_post( $chapo ) );
			if ( $txtFacultatif ) printf( '<p class="txt">%s</p>', wp_kses_post( $txtFacultatif ) );
			if ( $meta_auteur ) printf( '<p class="auteur">%s</p>', wp_kses_post( $meta_auteur ) );
		?>
	</div>

	<?php
		$meta_video = get_post_meta( $id, 'meta-video', true );

		for ( $i = 1; $i <= 7; $i++ ) {
			$section_content = get_post_meta( $id, "_wp_editor_section_${i}", true );
			$section_title   = get_post_meta( $id, "meta-title_${i}", true );
			$section_image   = get_post_meta( $id, "meta-image_${i}", true );
			$section_caption = get_post_meta( $id, "section${i}_caption", true );
			if ( ! empty( $section_title ) && ! empty( $section_content ) ) { 
				?>
					<div class="geoformat-section" id="geoformat_section_<?php echo $i; ?>">
						<header class="geoformat-section-header">
							<?php 
								if ( $section_image ) {
									$caption = ! empty( $section_caption ) ? sprintf( '<figcaption>%s</figcaption>', wp_kses_post( $section_caption ) ) : '';
									printf( '<figure><img src="%s" />%s</figure>', esc_url( $section_image ), $caption );
								} 
							?>
						</header>
						<div class="geoformat-section-content">
							<?php 
								printf( '<h2>%s</h2>', $section_title );
								echo apply_filters( 'the_content', $section_content ); 
							?>
						</div>
					</div>
				<?php 
			}
		} 
	?>
</section>