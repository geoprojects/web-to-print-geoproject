<?php
/**
 * Template for displaying project site info
 */
?>
<section class="cover site-info" id="site-info">
    <h1 class="cover-title"><?php echo esc_html( get_bloginfo( 'name' ) ); ?></h1>
    <p class="cover-subtitle description"><?php echo esc_html( get_bloginfo( 'description' ) ) ?></p>
    <p class="cover-url url"><?php echo esc_html( get_bloginfo( 'url' ) ) ?></p>
</section>