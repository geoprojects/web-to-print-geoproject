<?php
/**
 * Template for displaying settings sidebar
 */
$permalink = add_query_arg( 'paged', 'yes', get_permalink() );
$settings  = geoprint_get_template_settings( get_the_id() );
$sections  = geoprint_get_settings_sections();
?>
<aside id="pagedjs-settings">
    <button class="form-toggle form-open" aria-expanded="true" aria-controls="settings-form">
        <span class="screen-reader-text"><?php esc_html_e( 'Open settings', 'geoprint' ); ?></span>
        <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="currentColor" d="M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z" /></svg>
    </button>
    <div id="settings-form" class="open">
        <button class="form-toggle form-close" aria-expanded="true" aria-controls="settings-form">
            <span class="screen-reader-text"><?php esc_html_e( 'Close settings', 'geoprint' ); ?></span>
            <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="currentColor" d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" /></svg>
        </button>
        <form class="pagedjs-settings" method="POST" action="<?php echo esc_url( $permalink );?>">
            <h1><?php esc_html_e( 'Project settings', 'geoprint' ) ?></h1>
    
            <div class="settings-section content-section">
                <h2 class="settings-section-title">
                    <span><?php esc_html_e( 'Content settings', 'geoprint' ); ?></span>
                    <button class="section-toggle" aria-expanded="true" type="button" aria-controls="content-settings">
                        <span class="screen-reader-text"><?php esc_html_e( 'Expand settings', 'geoprint' ); ?></span>
                        <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="currentColor" d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z" /></svg>
                    </button>
                </h2>
                <div id="content-settings" class="settings open">
                    <?php 
                        geoprint_content_order_inputs();
                        geoprint_format_input();
                        geoprint_css_template_input();
                        geoprint_generate_cover_content_input();
                        geoprint_generate_colophon_input();
                        geoprint_hidden_content_input();
                        geoprint_custom_css_input();
                    ?>
                </div>
            </div>
    
            <?php foreach ( $sections as $key => $label ) : $selector = 'base' === $key ? ':root' : sprintf( '.pagedjs_%s_page', $key ) ;  ?>
                <div class="settings-section <?php echo esc_attr( $key ); ?>-section" data-selector="<?php echo esc_attr( $selector ); ?>">
                    <h2 class="settings-section-title">
                        <span class="section-title"><?php echo esc_html( $label ); ?></span>
                        <button class="section-toggle" aria-expanded="false" type="button" aria-controls="<?php echo esc_attr( $key ); ?>-settings">
                            <span class="screen-reader-text"><?php esc_html_e( 'Expand settings', 'geoprint' ); ?></span>
                            <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="currentColor" d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z" /></svg>
                        </button>
                    </h2>
                    <div id="<?php echo esc_attr( $key ); ?>-settings" class="settings section-settings">
                        <?php 
                            $sections_settings = array_filter( $settings, function($setting) use( $key ){ return ! empty( $setting['section'] ) && $key === $setting['section']; });
                            foreach ( $sections_settings as $id => $setting ) geoprint_render_setting_field( $id, $setting );
                        ?>
                    </div>
                </div>
            <?php endforeach; ?>
            
            <input type="hidden" name="geoprint_map_data" />
            <?php wp_nonce_field( 'geoprint_save_template_settings', 'geoprint_save_template_settings_nonce', true, true ); ?>
            <p>
                <input type="submit" value="<?php echo esc_attr( 'Save and refresh preview', 'geoprint' ); ?>" />
                <input type="button" id="print" value="<?php echo esc_attr( 'Print', 'geoprint' ); ?>" />
            </p>
        </form>
    </div>
</aside>