<?php
// delete_post_meta( get_the_id(), 'geoprint_settings' );
if( ! empty( $_POST ) ) geoprint_save_template_settings( get_the_id() );
$styles  = geoprint_get_template_styles();
$scripts = geoprint_get_template_scripts();
$inline_styles = geoprint_get_template_inline_styles();
$format = get_post_meta( get_the_id(), 'geoprint_book_format', true );
$format_url = ! empty( $format ) ? geoprint_get_format_url( $format ) : '';
?>

<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" >
        <?php foreach ( $styles as $id => $style ) printf( '<link id="%s" type="text/css" rel="stylesheet" href="%s" media="%s"/>', esc_attr( $id ), esc_url( $style['href'] ), esc_attr( $style['media'] ) ); ?>
        <?php foreach ( $inline_styles as $id => $css ) printf( '<style id="%s">%s</style>', esc_attr( $id ), $css ); ?>
    </head>
    <body>
        <?php include geoprint_get_template( 'settings' ); ?>     
        <?php include geoprint_get_template( 'source' ); ?>
        <script>
            var formatStyles = "<?php echo $format_url; ?>";
            var baseStyles = "<?php echo GEOPRINT_URL . 'build/css/base.css'; ?>";
        </script>
        <?php foreach ( $scripts as $id => $script ) printf( '<script id="%s" type="text/javascript" src="%s" /></script>', esc_attr( $id ), esc_url( $script['src'] ) ); ?>
    </body>
</html>